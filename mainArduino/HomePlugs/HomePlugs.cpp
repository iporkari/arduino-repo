/*
  HomePlugs.cpp - Library for interfacing with HomePlugs app on Android
  Created by Giacomo Bonfà and Matteo Pulega, December 21, 2016
  Released into the public domain.
*/

#include "Arduino.h"
#include "HomePlugs.h"
	
Program::Program(){
this->pos = -1;
this->startHour = 0;
this->startMinute = 0;
this->endHour = 0;
this->endMinute = 0;
for (int i = 0; i<7; i++){
	week[i] = false;
	}
	this->valid = false;
}

Program::Program(int pos, int startHour, int startMinute, int endHour, int endMinute, boolean monday, boolean tuesday, boolean wednesday, boolean thursday, boolean friday, boolean saturday, boolean sunday) {
  this->pos = pos;
  this->startHour = startHour;
  this->startMinute = startMinute;
  this->endHour = endHour;
  this->endMinute = endMinute;
  this->week[1] = monday;
  this->week[2] = tuesday;
  this->week[3] = wednesday;
  this->week[4] = thursday;
  this->week[5] = friday;
  this->week[6] = saturday;
  this->week[0] = sunday;
  this->valid = true;
}

int Program::getPos() {
  return this->pos;
}
int Program::getStartHour() {
  return this->startHour;
}
int Program::getStartMinute() {
  return this->startMinute;
}
int Program::getEndHour() {
  return this->endHour;
}
int Program::getEndMinute() {
  return this->endMinute;
}
bool Program::getRepeat(int index) {
  return this->week[index];
}
bool Program::repeat(){
  if (week[0] || week[1] || week [2] || week[3] || week[4] || week[5] || week[6])
    return true;
  else false;
}
bool Program::neverRepeat() {
  if (week[0] && week[1] && week [2] && week[3] && week[4] && week[5] && week[6])
    return false;
  else true;
}
bool Program::alwaysRepeat(){
  if (week[0] && week[1] && week [2] && week[3] && week[4] && week[5] && week[6])
    return true;
  else false;
}

static Program Program::parseString(String message){
  if (message.startsWith("_POS:")) {
        message.remove(0, 5);                                    // elimino la stringa "_POS:"
        int pos = message.substring(0, 2).toInt();
        message.remove(0, 2);                                    // elimino il dato "nn"
        if ( message.startsWith("+ON:") ) {
          message.remove(0, 4);                                  // elimino la stringa "+ON:"
          int startHour = message.substring(0, 2).toInt();
          int startMinute = message.substring(3, 5).toInt();
          message.remove(0, 5);                                  // elimino la stringa "hh:mm"
          if ( message.startsWith("-OFF:")) {
            message.remove(0, 5);                                // elimino la stringa "-OFF:"
            int endHour = message.substring(0, 2).toInt();
            int endMinute = message.substring(3, 5).toInt();
            message.remove(0, 5);                                // elimino la stringa "hh:mm"
            if ( message.startsWith("*WEEK:")) {
              message.remove(0, 6);                              // elimino la stringa "*WEEK:"
              boolean monday = Program::stringTOboolean(message.substring(0, 1));
              boolean tuesday = Program::stringTOboolean(message.substring(2, 3));
              boolean wednesday = Program::stringTOboolean(message.substring(4, 5));
              boolean thursday = Program::stringTOboolean(message.substring(6, 7));
              boolean friday = Program::stringTOboolean(message.substring(8, 9));
              boolean saturday = Program::stringTOboolean(message.substring(10, 11));
              boolean sunday = Program::stringTOboolean(message.substring(12, 13));
              message.remove(0, 13);                             // elimino la stringa "Y N Y N Y N Y"

              Program p (pos, startHour, startMinute, endHour, endMinute, monday, tuesday, wednesday, thursday, friday, saturday, sunday);
              return p;
            }
          }
        }
      }
      Program p;
      return p;
}

boolean Program::isValid() {
	return this->valid;
}

static boolean Program::stringTOboolean(String toConvert){	
  if (toConvert.equalsIgnoreCase("Y")) return true;
  if (toConvert.equalsIgnoreCase("N")) return false;
}



