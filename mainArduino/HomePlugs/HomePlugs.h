/*
  HomePlugs.h - Library for interfacing with HomePlugs app on Android
  Created by Giacomo Bonfà and Matteo Pulega, December 21, 2016
  Released into the public domain.
*/
#ifndef HomePlugs_h
#define HomePlugs_h

#include "Arduino.h"

class Program
{
  public:
    Program(int pos, int startHour, int startMinute, int endHour, int endMinute, boolean monday, boolean tuesday, boolean wednesday, boolean thursday, boolean friday, boolean saturday, boolean sunday);
    Program ();
    int getPos();
    int getStartHour();
    int getStartMinute();
    int getEndHour();
    int getEndMinute();
    boolean getRepeat(int index);
    boolean repeat();
    boolean neverRepeat();
    boolean alwaysRepeat();
    static Program parseString(String message);
    boolean isValid();
	static boolean stringTOboolean (String toConvert);
	
  private:
    int pos;
    int startHour;
    int startMinute;
    int endHour;
    int endMinute;
    boolean valid;
    boolean week[7];
};

#endif
