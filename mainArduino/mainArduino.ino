#include <LinkedList.h>
#include <Time.h>
#include <TimeLib.h>
#include <HomePlugs.h>

/* pin Arduino nano utilizzati
  LED integrato = 13
  pin TX (da arduino a bluetooth) = 10
  pin RX (da bluetooth ad arduino) = 11
  NB!! TXarduino=RXbluetooth & RXarduino=TXbluetooth
  codifica date/time = TIME_hh:mm_gg/mm/aaaa
  codifica programmazione = "PROG:nn_POS:nn+ON:hh:mm-OFF:hh:mm*WEEK:Y|N Y|N Y|N Y|N Y|N Y|N Y|N" (45 chars)
*/

#include <SoftwareSerial.h>

int rxPin = 11;
int txPin = 10;
SoftwareSerial bluetooth(rxPin, txPin);

String message; //string that stores the incoming message
String state = "OFF";
char command;
const int relay = 13;
int numberOfProgram = 0;

LinkedList<Program> myProgramList;

void ON () {
  digitalWrite(relay, HIGH);
  state = "ON";
  return;
}

void OFF () {
  digitalWrite(relay, LOW);
  state = "OFF";
  return;
}

void setup()
{
  Serial.begin(9600);
  bluetooth.begin(9600); //set baud rate
  pinMode(relay, OUTPUT);
  msg("Arduino is ready");
  myProgramList = LinkedList<Program>();
}

void msg (String s) {
  Serial.println (s);
}

void loop()
{
  while (bluetooth.available()) {
    command = (byte) bluetooth.read();
    message += command;
    msg("Waiting");
    msg(message);
    delay(1);
  }

  if ( message.startsWith("ON")) {
    ON();
    delay(20);
    msg("messaggio ricevuto: " + message + " stato interno: " + state);
    message = "";
    delay (500);
  }
  if (message.startsWith("OFF")) {
    OFF();
    delay(20);
    msg("messaggio ricevuto: " + message + " stato interno: " + state);
    message = "";
    delay (500);
  }
  if (message.startsWith("TIME")) {
    // TIME_hh:mm:ss_gg/mm/aaaa
    msg(message);
    int hour = message.substring(5, 7).toInt();
    int minute = message.substring(8, 10).toInt();
    int second = message.substring(11, 13).toInt();
    int day = message.substring(14, 16).toInt();
    int month = message.substring(17, 19).toInt();
    int year = message.substring(20, 24).toInt();
    setTime(hour, minute, second, day, month, year);
    digitalClockDisplay();
    message = "";
  }

  if (message.startsWith("PROG")) {
    message.remove(0, 5);                                        // elimino la stringa "PROG:"
    numberOfProgram = message.substring(0, 2).toInt();
    message.remove(0, 2);                                        // elimino il dato "nn"
    myProgramList.clear();
    for ( int i = 0; i < numberOfProgram ; i++) {
      Program toAdd = Program::parseString(message.substring(0, 45));
      if (toAdd.isValid())
        myProgramList.add(toAdd);
      else
        numberOfProgram--;
      message.remove(0, 45);
      //"_POS:nn+ON:hh:mm-OFF:hh:mm*WEEK:Y N Y N Y N Y" //45chars
    }
    message = "";
  }

  for (int i = 0; i < numberOfProgram; i++) {
    exec(i);
  }
}

void digitalClockDisplay()
{
  // digital clock display of the time and date
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.print(" - ");
  Serial.print(day());
  Serial.print("/");
  Serial.print(month());
  Serial.print("/");
  Serial.println(year());
}

void printDigits(int digits)
{
  Serial.print(":");
  if (digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

void exec (int index) {
  Program p = myProgramList.get(index);
  int actualHour = hour();          // The hour now  (0-23)
  int actualMinute = minute();      // The minute now (0-59)
  int actualDay = weekday();        // Day of the week, Sunday is day 1 | Monday 2 | Tuesday 3 | Wednesday 4 | Thursday 5 | Friday 6 | Saturday 7
  int startHour = p.getStartHour();
  int startMinute = p.getStartMinute();
  int endHour = p.getEndHour();
  int endMinute = p.getEndMinute();

  if (p.neverRepeat()) {
    if (sameTime(actualHour, actualMinute, startHour, startMinute)) {
      ON();
      myProgramList.remove(index);
      delay(10000);
      return;
    }
    if (sameTime(actualHour, actualMinute, endHour, endMinute) ) {
      OFF();
      myProgramList.remove(index);
      delay(10000);
      return;
    }
  }
  if (p.alwaysRepeat()) {
    if (sameTime(actualHour, actualMinute, startHour, startMinute)) {
      ON();
      delay(10000);
      return;
    }
    if (sameTime(actualHour, actualMinute, endHour, endMinute) ) {
      OFF();
      delay(10000);
      return;
    }
  }
  if (p.repeat()) {
    if (sameTime(actualHour, actualMinute, startHour, startMinute)) {
      if (p.getRepeat(actualDay - 1)) {
        ON();
        delay(10000);
        return;
      }
    }
    if (sameTime(actualHour, actualMinute, endHour, endMinute) ) {
      if (p.getRepeat(actualDay - 1)) {
        OFF();
        delay(10000);
        return;
      }
    }
  }
}

bool sameTime(int hour1, int minute1, int hour2, int minute2) {
  if (hour1 == hour2 && minute1 == minute2) {
    return true;
  } else {
    return false;
  }
}


